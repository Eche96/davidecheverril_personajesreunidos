﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample1_CubeInfo : MonoBehaviour
{
    [Header ("Lover1")]
    [SerializeField]
    private Characters_Actions action1 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action2 = Characters_Actions.No_Action;

    [Header("Lover2")]
    [SerializeField]
    private Characters_Actions action3 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action4 = Characters_Actions.No_Action;

    public Sample1_CubeActions GetActionsLover1()
    {
        
        Sample1_CubeActions actions = new Sample1_CubeActions();
        actions.action1 = action1;
        actions.action2 = action2;
        return actions;
    }

    public Sample1_CubeActions GetActionsLover2()
    {
        
        Sample1_CubeActions actions = new Sample1_CubeActions();
        actions.action1 = action3;
        actions.action2 = action4;
        return actions;
    }
}
