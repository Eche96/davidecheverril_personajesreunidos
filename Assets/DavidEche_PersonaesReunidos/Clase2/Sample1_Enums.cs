﻿public enum Gameplay_States
{
    None = -1,
    Checking = 0,
    Movement = 1,
    Win = 2
}

public enum Characters_Actions
{
    No_Ground = -2,
    No_Action = -1,
    Move_Forward = 0,
    Move_Win = 1,
    Turn_Left = 2,
    Turn_Right = 3,
    Turn_Forward = 4
}

public enum Characters
{
    Lover1 = 0,
    Lover2 = 1
}
